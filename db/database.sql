-- Active: 1691609037162@@127.0.0.1@3306@companydb
CREATE DATABASE IF NOT EXISTS companydb;

USE companydb;

CREATE TABLE employee (
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) DEFAULT NULL,
    salary INT(5) DEFAULT NULL,
    PRIMARY KEY (id)
);

DESCRIBE employee;

INSERT INTO employee VALUES 
    (1, 'Joe', 1000),
    (2, 'Mileer', 1500),
    (3, 'Laura', 3000),
    (4, 'Daniela', 2000);